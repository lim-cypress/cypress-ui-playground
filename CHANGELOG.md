### Release Version
**1.1.0**
- Fixing the broken CI pipeline

**1.0.0**
- Project Creation
- Installed Cypress and Wrote Initial Test
- Installed Cypress Code Coverage
- Setup a CI/CD pipeline