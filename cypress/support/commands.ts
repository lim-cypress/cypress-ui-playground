import 'cypress-wait-until';
import '@cypress/code-coverage/support';

declare global {
  namespace Cypress {
    interface Chainable<Subject = any> {
      navigate: (url?: string) => Chainable;
    }
  }
}

Cypress.Commands.add('navigate', (url) => {
  cy.log(`Navigating to ${url}`);
  return cy.visit('http://localhost:4200' + url);
});
