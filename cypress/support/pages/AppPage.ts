export default class AppPage {
  content = '.content';
  angularLogo = 'img';
  welcomeToolBar = '.toolbar > span';
  twitterLogo = '#twitter-logo';
  youtubeLogo = '#youtube-logo';

  rocketIcon = '#Path_34';
  highlightCardText = '.highlight-card > span';

  resourceHeader = '.content > :nth-child(2)';
  resourceSubHeader = '.content > :nth-child(3)';
}

export const appPage = new AppPage();
