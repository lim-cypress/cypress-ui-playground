/// <reference types="cypress" />
import { appPage } from '../support/index';
const translation = require('../fixtures/locale-en.json');
const userRoles = ['Admin', 'Guest'];
const sizes = ['iphone-6', 'ipad-2', [1024, 768], 'iphone-xr', 'samsung-note9', 'samsung-s10'];

describe('Angular Sample Test Suite', () => {
  userRoles.forEach((roles) => {
    sizes.forEach((size) => {
      context(`${roles}`, () => {
        context(`on ${size} screen`, () => {
          beforeEach(() => {
            /**
             * Changing App Screensize
             */
            if (Cypress._.isArray(size)) {
              cy.viewport(size[0], size[1]);
            } else {
              // @ts-ignore
              cy.viewport(size);
            }

            /**
             * Changing User Role
             */
            if (Cypress._.isArray('Admin')) {
              cy.navigate('/').log('Logging in as Admin');
            } else {
              cy.navigate('/').log('Logging in as Guest');
            }
          });

          it('should create the app', () => {
            cy.log('creating component instance...')
              .get(appPage.content)
              .should('be.visible');
          });

          it(`should have as title 'angular-cypress-codecoverage'`, () => {
            cy.log('creating component instance...')
              .title()
              .should('eq', translation.title);
          });

          it('should render title', () => {
            cy.log('creating component instance...')
              .get(appPage.highlightCardText)
              .should('be.visible')
              .and('contains.text', translation.contentText);
          });
        });
      });
    });
  });
});
