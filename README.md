# AngularCypressCodecoverage

This project purpose is to demonstrate how to install and use Cypress, make Cypress Code Coverage Work in Angular, 
run a Test Suite with multiple user and Screen Size and finally using CI to run both Unit Test and Cypress Test.

## Installation
To install all the `node_modules` you can do a `npm install`.

## List of node packages used for this project.

```
npm install --save-dev cypress
npm install --save-dev @briebug/cypress-schematic
npm install --save-dev @cypress/webpack-preprocessor
npm install --save-dev ngx-build-plus
npm install --save-dev istanbul-instrumenter-loader
npm install --save-dev @istanbuljs/nyc-config-typescript source-map-support ts-node
npm install --save-dev @cypress/code-coverage nyc istanbul-lib-coverage
npm install --save-dev @angular-builders/custom-webpack
npm install --save-dev karma-junit-reporter
npm install --save-dev ts-loader
npm install --save-dev cypress-wait-until
```
## NPM Scripts Command
```
start - To Start the service of the app
prebuild - Being run first before the build command
build - To Compile the all the files and dump it to the dist folder
test - To run the unit test with coverage
test:watch - To run the unit test and open a chrome browser. Then every code change it updates the test
lint - Runs the TS-Lint to make sure the codes are compliance
cy:coverage - Checks the Cypress Code Coverage if its 80
cy:open - Run Cypress with a Test Runner
cy:run - Run Cypress headless mode but with Electron
cy:ci:chrome: Run Cypress headless mode with Chrome and in Parallel
cy:ci:firefox: Run Cypress headless mode with Firefox and in Parallel
```

## Code Changes Needed for Cypress Code Coverage
`package.json` </br>
This is used for excluding and including files for the Coverage.
```
  "nyc": {
    "extends": "@istanbuljs/nyc-config-typescript",
    "all": true,
    "exclude": [
      "cypress",
      "coverage",
      "dist",
      "src/main.ts",
      "src/app/app.component.spec.ts",
      "karma.conf.js",
      "src/test.ts",
      "src/environments/environment.prod.ts"
    ],
    "excludeAfterRemap": true
  }
```

`cypress/coverage.webpack.js`</br>
This is needed to run Cypress with a Custom Webpack.
```
module.exports = {
  module: {
    rules: [
      {
        test: /\.(js|ts)$/,
        loader: 'istanbul-instrumenter-loader',
        options: { esModules: true },
        enforce: 'post',
        include: require('path').join(__dirname, '..', 'src'),
        exclude: [
          /\.(e2e|spec)\.ts$/,
          /node_modules/,
          /(ngfactory|ngstyle)\.js/
        ]
      }
    ]
  }
};
```
`cypress/plugins/cy-ts-preprocessor.js`</br>
It is used for Cypress preprocessor for bundling JavaScript via webpack.
```
const wp = require('@cypress/webpack-preprocessor')

const webpackOptions = {
  resolve: {
    extensions: ['.ts', '.js']
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: [/node_modules/],
        use: [
          {
            loader: 'ts-loader'
          }
        ]
      }
    ]
  }
}

const options = {
  webpackOptions
}

module.exports = wp(options)
``` 
`cypress/plugins/index.js`</br>
This is where export the code coverage plugin.
```
const cypressTypeScriptPreprocessor = require("./cy-ts-preprocessor");
const registerCodeCoverageTasks = require('@cypress/code-coverage/task');

module.exports = (on, config) => {
  on('file:preprocessor', cypressTypeScriptPreprocessor);
  registerCodeCoverageTasks(on, config);
  return config;
}
```

`cypress/support/commands.js` </br>
It is needed to be put the import of the coverage, so that it can be use in all Cypress Test
```
import '@cypress/code-coverage/support';
```

## Resources
https://docs.cypress.io/guides/overview/why-cypress